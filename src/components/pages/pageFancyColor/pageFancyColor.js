import React from 'react';
import SliderRange from '../../filters/sliders/sliderRange';
import SliderRangeCaratWeight from '../../filters/sliders/sliderRangeCaratWeight';
import FilterColor from '../../filters/filterColor/filterColor';
import SliderRangeIntensity from '../../filters/sliders/sliderRangeIntensity';
import SliderRangeClarity from '../../filters/sliders/sliderRangeClarity';
import SliderRangeFluoStrength from '../../filters/sliders/sliderRangeFluoStrength';
import SelectAuction from '../../filters/selectAuction/selectAuction';
import CutShapeFilters from '../../filters/cutShapeFilters/cutShapeFilters';
import SelectMultiSort from '../../filters/selectMultiSort/selectMultiSort';
import PanelActions from '../../panelActions/panelActions';
import BlockStone from "../../blocks/blockStone/blockStone";

class PageFancyColor extends React.Component {
    render() {
        return (
            <div className="page">
                <div className="page__top__filters">
                    <div className="container-inner">
                        <div className="page__top__filters__content">
                            <div className="row row-lg">
                                <div className="col-md-6 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <CutShapeFilters/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter filter-slider">
                                            <h5 className="filter__label">Carat weight</h5>
                                            <SliderRangeCaratWeight />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="filter">
                                                    <h5 className="filter__label">Certificate</h5>
                                                    <div className="filter__bottom">
                                                        <div className="checkboxes">
                                                            <div className="checkbox">
                                                                <label>
                                                                    <input type="checkbox" />
                                                                    <span className="checkbox__text">GIA</span>
                                                                </label>
                                                            </div>
                                                            <div className="checkbox">
                                                                <label>
                                                                    <input type="checkbox" />
                                                                    <span className="checkbox__text">IIDGR</span>
                                                                </label>
                                                            </div>
                                                            <div className="checkbox">
                                                                <label>
                                                                    <input type="checkbox" />
                                                                    <span className="checkbox__text">HRD</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="filter">
                                                    <h5 className="filter__label">Lot number</h5>
                                                    <div className="filter__bottom">
                                                        <input type="search" placeholder="Type number" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-lg">
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter filter-color">
                                            <h5 className="filter__label">Color</h5>
                                            <FilterColor />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter">
                                            <h5 className="filter__label">Intensity</h5>
                                            <SliderRangeIntensity />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter">
                                            <h5 className="filter__label">Clarity</h5>
                                            <SliderRangeClarity />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter">
                                            <h5 className="filter__label">Fluorescence strength</h5>
                                            <SliderRangeFluoStrength />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-lg">
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter">
                                            <h5 className="filter__label">Auction</h5>
                                            <SelectAuction/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter">
                                            <h5 className="filter__label">Seller</h5>
                                            <div className="filter__bottom">
                                                <div className="checkboxes">
                                                    <div className="checkbox">
                                                        <label>
                                                            <input type="checkbox" />
                                                            <span className="checkbox__text">De Beers Auction Sales</span>
                                                        </label>
                                                    </div>
                                                    <div className="checkbox">
                                                        <label>
                                                            <input type="checkbox" />
                                                            <span className="checkbox__text">De Beers Auction Sales  Registered Seller</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter">
                                            <h5 className="filter__label">Symmetry</h5>
                                            <SliderRange />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 col-larger">
                                    <div className="col-larger__inner">
                                        <div className="filter">
                                            <h5 className="filter__label">Polish</h5>
                                            <SliderRange />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="page__top__filters__bottom">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="results-found">Products found: <b>92</b> of <span>66500</span></div>
                                </div>
                                <div className="col-md-6 to-right">
                                    <a className="reset-filters">
                                        <span className="reset-filters__icon">&times;</span>
                                        <span className="reset-filters__text">Reset filters</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="page__main">
                    <div className="page__main__top">
                        <div className="container-inner">
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="filter filter-sort">
                                        <h5 className="filter__label">Sort</h5>
                                        <SelectMultiSort/>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="filter">
                                        <h5 className="filter__label">Show</h5>
                                        <div className="filter__bottom">
                                            <div className="checkboxes">
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" />
                                                        <span className="checkbox__text">Grayscale</span>
                                                    </label>
                                                </div>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" />
                                                        <span className="checkbox__text">Single scale</span>
                                                    </label>
                                                </div>
                                                <div className="checkbox">
                                                    <label>
                                                        <input type="checkbox" />
                                                        <span className="checkbox__text">Descriptions</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    <div className="filter filter-compare">
                                        <div className="filter__bottom">
                                            <span className="filter-compare__icon"></span>
                                            <span className="filter-compare__text">Compare selected</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-2 to-right">
                                    <div className="filter filter-page-view">
                                        <div className="filter-page-view__item">
                                            <button className="filter-page-view__item__icon icon-combo"></button>
                                        </div>
                                        <div className="filter-page-view__item">
                                            <button className="filter-page-view__item__icon icon-table"></button>
                                        </div>
                                        <div className="filter-page-view__item">
                                            <button className="filter-page-view__item__icon icon-big-img"></button>
                                        </div>
                                        <div className="filter-page-view__item">
                                            <button className="filter-page-view__item__icon icon-small-img"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="page__main__middle">
                        <div className="container-inner">
                            <PanelActions/>
                            <div className="row row-md-larger">
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                                <div className="block-stone__wrap col-xs-6 col-sm-4 col-md-3 col-md-2-larger">
                                    <BlockStone/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PageFancyColor;