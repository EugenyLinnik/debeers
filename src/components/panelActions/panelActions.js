import React from 'react';

class PanelActions extends React.Component {
    render() {
        return (
            <div className="panel-actions">
                <div className="panel-actions__inner">
                    <div className="panel-actions__item play">
                        <span className="glyphicon glyphicon-play"></span>
                        <span className="glyphicon glyphicon-pause"></span>
                    </div>
                    <div className="panel-actions__item front-view"></div>
                    <div className="panel-actions__item back-view"></div>
                    <div className="panel-actions__item right-view"></div>
                    <div className="panel-actions__item left-view"></div>
                    <div className="panel-actions__item full-view"></div>
                </div>
            </div>
        );
    }
}

export default PanelActions;