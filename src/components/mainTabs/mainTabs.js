import React from 'react';
import { NavLink } from 'react-router-dom';
import './main-tabs.less';

class MainTabs extends React.Component {
    render() {
        return (
            <div className="page__tabs">
                <div className="container-inner">
                    <ul className="nav nav-tabs nav-justified">
                        <li><NavLink exact={true} to={{ pathname: '/' }} activeClassName='active'>Colorless Polished</NavLink></li>
                        <li><NavLink to={{ pathname: '/fancy-color' }} activeClassName='active'>Fancy Color Polished</NavLink></li>
                        <li><NavLink to={{ pathname: '/rough' }} activeClassName='active'>Rough</NavLink></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default MainTabs;