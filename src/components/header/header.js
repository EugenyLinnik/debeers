import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import logo from './logo.png';
import './header.less';

class Header extends Component {
    render() {
        return (<div className="page-header">
            <div className="container-inner">
                <div className="row">
                    <div className="page-header__left col-md-3 col-sm-4">
                        <NavLink exact={true} to={{ pathname: '/' }}>
                            <img className="page-header__logo" src={logo} alt="DeBeers logo" />
                        </NavLink>
                    </div>
                    <div className="page-header__right col-md-9 col-sm-8">
                        <nav className="page-header__nav">
                            <a className="page-header__nav__link">Bid now</a>
                            <a className="page-header__nav__link">Schedule viewing</a>
                            <a className="page-header__nav__link page-header__nav__link-signin">Sign in</a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>);
    }
}

export default Header;