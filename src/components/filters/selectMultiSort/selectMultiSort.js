import React from 'react';

class SelectMultiSort extends React.Component {
    render() {
        return (
            <div className="filter__bottom">
                <div className="filter-sort__item active asc">Media Relevance</div>
                <div className="filter-sort__item">Date</div>
                <div className="filter-sort__item">Price</div>
                <div className="filter-sort__item">Carat</div>
                <div className="filter-sort__item active desc">Color</div>
                <div className="filter-sort__item">Clarity</div>
            </div>
        );
    }
}

export default SelectMultiSort;