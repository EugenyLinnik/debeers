import React from 'react';
import Select from 'react-select';

const selectOptions = [
    { value: 1, label: 'Cycle 1.1 - 1.11 & 1.12 ' },
    { value: 2, label: 'Cycle 2.1 - 1.11 & 1.12 ' }
];

class SelectAuction extends React.Component {
    render() {
        return (
            <div className="filter__select">
                <Select name="form-field-name" value="select range" options={selectOptions} />
            </div>
        );
    }
}

export default SelectAuction;