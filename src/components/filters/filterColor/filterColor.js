import React from 'react';

class FilterColor extends React.Component {
    render() {
        return (
            <div className="filter__bottom">
                <div className="filter-color__item">
                    <div className="filter-color__item__inner yellow">Yellow</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner pink">Pink</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner blue">Blue</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner green">Green</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner orange">Orange</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner brown">Brown</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner red">Red</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner purple">Purple</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner black">Black</div>
                </div>
                <div className="filter-color__item">
                    <div className="filter-color__item__inner grey">Grey</div>
                </div>
            </div>
        );
    }
}

export default FilterColor;