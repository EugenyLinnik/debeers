import React from 'react';
import Slider from 'rc-slider';

const marks = {
    0: 'None',
    25: 'Faint',
    50: 'Medium',
    75: 'Strong',
    100: 'Very Strong'
};

function log(value) {
    console.log(value); //eslint-disable-line
}

class SliderRangeFluoStrength extends React.Component {
    render() {
        return (
            <div className="filter__bottom">
                <Slider.Range min={0} marks={marks} step={5} onChange={log} defaultValue={[0, 20]} />
            </div>
        );
    }
}

export default SliderRangeFluoStrength;