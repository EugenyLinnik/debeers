import React from 'react';
import Slider from 'rc-slider';

const marks = {
    0: 'FL',
    14.2857: 'IF',
    28.5714: 'VVS1',
    42.8571: 'VVS2',
    57.1428: 'VS1',
    71.4285: 'VS2',
    85.7142: 'S11',
    100: 'S12'
};

function log(value) {
    console.log(value); //eslint-disable-line
}

class SliderRangeClarity extends React.Component {
    render() {
        return (
            <div className="filter__bottom">
                <Slider.Range min={0} marks={marks} step={7} onChange={log} defaultValue={[0, 14.2857]} />
            </div>
        );
    }
}

export default SliderRangeClarity;