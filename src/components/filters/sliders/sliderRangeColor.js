import React from 'react';
import Slider from 'rc-slider';

const marks = {
    0: 'D',
    14.2857: 'E',
    28.5714: 'F',
    42.8571: 'G',
    57.1428: 'H',
    71.4285: 'I',
    85.7142: 'J',
    100: 'K'
};

function log(value) {
    console.log(value);
}

class SliderRangeColor extends React.Component {
    render() {
        return (
            <div className="filter__bottom">
                <Slider.Range min={0} marks={marks} step={7} onChange={log} defaultValue={[14.2857, 42.8571]} />
            </div>
        );
    }
}

export default SliderRangeColor;