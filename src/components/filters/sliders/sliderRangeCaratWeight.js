import React from 'react';
import Slider from 'rc-slider';
import Select from 'react-select';

const selectOptions = [
    { value: 'one', label: 'One' },
    { value: 'two', label: 'Two' }
];

const Range = Slider.Range;

class SliderRangeCaratWeight extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            min: 0,
            max: 10,
        };
    }
    onSliderChange = (value) => {
        console.log(value);
    };
    onMinChange = (e) => {
        this.setState({
            min: +e.target.value || 0,
        });
    };
    onMaxChange = (e) => {
        this.setState({
            max: +e.target.value || 10,
        });
    };
    render() {
        return (
            <div className="filter__bottom">
                <Range defaultValue={[0, 5]} min={this.state.min} max={this.state.max}
                       onChange={this.onSliderChange} />
                <div className="filter-slider__range">
                    <div className="row">
                        <div className="col-md-6">
                            <input type="number" value={this.state.min} onChange={this.onMinChange} />
                            <span className="divider">-</span>
                            <input type="number" value={this.state.max} onChange={this.onMaxChange} />
                        </div>
                        <div className="col-md-6 to-right">
                            <Select name="form-field-name" value="select range" options={selectOptions} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SliderRangeCaratWeight;