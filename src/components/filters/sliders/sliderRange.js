import React from 'react';
import Slider from 'rc-slider';

const marks = {
    0: 'Excellent',
    25: 'Very Good',
    50: 'Good',
    75: 'Fair',
    100: 'Poor'
};

function log(value) {
    console.log(value); //eslint-disable-line
}

class SliderRange extends React.Component {
    render() {
        return (
            <div className="filter__bottom">
                <Slider.Range min={0} marks={marks} step={5} onChange={log} defaultValue={[0, 20]} />
            </div>
        );
    }
}

export default SliderRange;