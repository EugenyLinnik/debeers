import React from 'react';
import Slider from 'rc-slider';

const marks = {
    0: <span className="visible-screen-l">Faint</span>,
    12.5: <span className="visible-screen-l">V.Light</span>,
    25: <span>Light</span>,
    37.5: <span className="visible-screen-l">F.Light</span>,
    50: <span className="visible-screen-l">Fancy</span>,
    62.5: <span className="visible-screen-l">Intense</span>,
    75: <span className="visible-screen-l">Vivid</span>,
    87.5: <span>Deep</span>,
    100: <span className="visible-screen-l">Deep</span>
};

function log(value) {
    console.log(value); //eslint-disable-line
}

class SliderRangeIntensity extends React.Component {
    render() {
        return (
            <div className="filter__bottom">
                <Slider.Range min={0} marks={marks} step={7} onChange={log} defaultValue={[0, 14.2857]} />
            </div>
        );
    }
}

export default SliderRangeIntensity;