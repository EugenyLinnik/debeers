import React, { Component } from 'react';
import Header from './header/header.js';
import Main from './main/main';
import MainTabs from './mainTabs/mainTabs';
import './App.less';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Header/>
          <div className="main-container">
              <MainTabs/>
              <Main/>
          </div>
      </div>
    );
  }
}

export default App;
