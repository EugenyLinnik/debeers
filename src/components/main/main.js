import React from 'react';
import { AnimatedSwitch } from 'react-router-transition/lib/react-router-transition';
import Route from 'react-router-dom/Route';
import PageFancyColor from '../pages/pageFancyColor/pageFancyColor';
import PageColorless from '../pages/pageColorless/pageColorless';
import PageRough from '../pages/pageRough/pageRough';

class Main extends React.Component {
    render() {
        return (
            <AnimatedSwitch
                atEnter={{ opacity: 0 }}
                atLeave={{ opacity: 0 }}
                atActive={{ opacity: 1 }}
                className="route-wrap"
            >
                <Route exact path='/' component={PageColorless}/>
                <Route path='/fancy-color' component={PageFancyColor}/>
                <Route path='/rough' component={PageRough}/>
            </AnimatedSwitch>
        );
    }
}

export default Main;