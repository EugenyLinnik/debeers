import React from 'react';
import './block-stone.less';
import picStone from '../../../img/temp/pic-stone.png';

class BlockStone extends React.Component {
    constructor() {
        super();

        this.state = { checked: false };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange() {
        this.setState({
            checked: !this.state.checked
        })
    }
    render() {
        const selected = this.state.checked ? 'block-stone selected' : 'block-stone';

        return (
            <div className={ selected }>
                <div className="block-stone__inner">
                    <div className="block-stone__top">
                        <div className="block-stone__top__actions">
                            <div className="block-stone__top__actions__item">
                                <span className="glyphicon glyphicon-facetime-video"></span>
                            </div>
                            <div className="block-stone__top__actions__item">
                                <span className="glyphicon glyphicon-camera"></span>
                            </div>
                            <div className="block-stone__top__actions__item">
                                <span className="glyphicon glyphicon-file"></span>
                            </div>
                        </div>
                        <div className="block-stone__img__wrap">
                            <div className="block-stone__img">
                                <img src={picStone} alt="" />
                            </div>
                        </div>
                    </div>
                    <div className="block-stone__middle">
                        <div className="block-stone__title"><a>Cycle 8. Lot 4001</a></div>
                        <div className="block-stone__description">EX Round, 3.61ct, G, VVS1, EX, EX, NON, -5.21%</div>
                    </div>
                    <div className="block-stone__bottom">
                        <div className="row">
                            <div className="col-md-4">
                                <div className="checkboxes">
                                    <div className="checkbox bk">
                                        <label>
                                            <input type="checkbox" checked={ this.state.checked } onChange={ this.handleChange } />
                                            <span className="checkbox__text"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-8 to-right">
                                <div className="block-stone__bottom__actions">
                                    <div className="block-stone__bottom__actions__item">
                                        <span className="label-new">New</span>
                                    </div>
                                    <div className="block-stone__bottom__actions__item">
                                        <span className="glyphicon glyphicon-star"></span>
                                    </div>
                                    <div className="block-stone__bottom__actions__item">
                                        <span className="glyphicon glyphicon-ban-circle"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default BlockStone;